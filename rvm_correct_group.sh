#Original:
# if groups  | grep -q rvm ; then
#   source "/usr/local/lib/rvm"
# fi

# Doesnt work, since
# $ groups => root
# $ groups `whoami` => root : root rvm
#

# This works:
if groups `whoami` | grep -q rvm ; then
  source "/usr/local/lib/rvm"
fi